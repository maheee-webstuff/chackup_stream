
function setup() {

  var imgs = document.querySelectorAll("div.image img");

  for (var i = 0; i < imgs.length; ++i) {
    imgs[i].onclick = function (e) {console.log(this);
      var parent = this.parentElement;
      parent.className = parent.className === "image clicked" ? "image" : "image clicked";
    };
  }

}

window.onload = setup;