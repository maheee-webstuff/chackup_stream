
<#macro render_postImage no tim ext showLink>
    <#assign imageSrc = "/images/" + no?c + "/" + tim?c + ext>

    <#if ext == ".webm">
    
        <div class="image">
            <!--
            <video controls>
                <source src="${imageSrc}">
            </video>
            -->
            <#if showLink>
                <a class="newTabLink" target="_blank" href="${imageSrc}"><div>in new tab</div></a>
            </#if>
        </div>
    
    <#else>
    
        <div class="image">
            <img src="${imageSrc}" />
            <#if showLink>
                <a class="newTabLink" target="_blank" href="${imageSrc}"><div>in new tab</div></a>
            </#if>
        </div>
    
    </#if>

</#macro>


<#macro render_searchResult result>
    <div class="post">
        <div class="title">
            <span>${result.lastModified?string.@asDateTime1000}</span>
            <span>${result.no?c}</span>
        </div>
        
        <#if result.firstExt??>
            <#if result.firstTim??>
                <@render_postImage no=result.no tim=result.firstTim ext=result.firstExt showLink=false/>
            </#if>
        </#if>

        <div class="text">
            ${result.firstText}
            <p><a href="/thread/${result.no?c}">Show Thread</a></p>
        </div>
    </div>
</#macro>


<#macro render_searchResultOverview result>

    <#assign timediff = (.now?long/1000 - result.lastModified) / 60>

    <#if timediff < 5>
        <#assign colorClass = "green">
    <#elseif timediff < 15>
        <#assign colorClass = "yellow">
    <#elseif timediff < 30>
        <#assign colorClass = "red">
    <#else>
        <#assign colorClass = "black">
    </#if>

    <div class="post overview ${colorClass}">
        <div class="title">
            <span>${result.lastModified?string.@asDateTime1000}</span>
            <span>${result.no?c}</span>
        </div>
        
        <#if result.firstExt??>
            <#if result.firstTim??>
                <a href="/thread/${result.no?c}">
                    <@render_postImage no=result.no tim=result.firstTim ext=result.firstExt showLink=false/>
                </a>
            </#if>
        </#if>

        <div class="text">
            <p>
                <a href="/thread/${result.no?c}">${result.title}</a>
            </p>
        </div>
    </div>
</#macro>


<#macro render_post thread post>
    <div class="post">
        <div class="title">
            <span>${post.time?string.@asDateTime1000}</span>
            <span>${post.no?c}</span>
        </div>
        <a name="p${post.no?c}"></a>

        <#if post.ext??>
            <#if post.tim??>
                <@render_postImage no=thread.no tim=post.tim ext=post.ext showLink=true/>
            </#if>
        </#if>

        <div class="text">
            ${post.com!""}
        </div>
    </div>
</#macro>
