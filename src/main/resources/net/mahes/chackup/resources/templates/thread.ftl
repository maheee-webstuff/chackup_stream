<#include "./parts/macros.ftl">

<#assign title = thread.posts[0].semantic_url>
<#include "./parts/header.ftl">

    
    
    <a target="_blank" href="https://boards.4chan.org/b/thread/${thread.no?c}/">Original</a>

    <#list thread.posts as p>

        <@render_post thread=thread post=p/>

    </#list>

<#include "./parts/footer.ftl">
