package net.mahes.chackup.server.template.utils;

import java.text.SimpleDateFormat;
import java.util.Locale;

import freemarker.core.Environment;
import freemarker.core.TemplateNumberFormat;
import freemarker.core.TemplateNumberFormatFactory;
import freemarker.core.TemplateValueFormatException;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateNumberModel;

public class DateTimeNumberFormat extends TemplateNumberFormatFactory {

    @Override
    public TemplateNumberFormat get(String params, Locale locale, Environment env) throws TemplateValueFormatException {
        return new TemplateNumberFormat() {

            @Override
            public String getDescription() {
                return null;
            }

            @Override
            public boolean isLocaleBound() {
                return false;
            }

            @Override
            public String formatToPlainText(TemplateNumberModel model) throws TemplateValueFormatException, TemplateModelException {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd, HH:mm:ss");
                return dateFormat.format(model.getAsNumber().longValue());
            }
        };
    }

}
