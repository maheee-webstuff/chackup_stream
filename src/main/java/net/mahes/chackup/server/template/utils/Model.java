package net.mahes.chackup.server.template.utils;

import java.util.List;

import net.mahes.chackup.model.SearchResult;
import net.mahes.chackup.model.ThreadModel;

public class Model {
    private ThreadModel thread;
    private List<SearchResult> searchResults;
    private StatusModel status;

    public Model(ThreadModel thread, List<SearchResult> searchResults, StatusModel status) {
        this.thread = thread;
        this.searchResults = searchResults;
        this.status = status;
    }

    public ThreadModel getThread() {
        return thread;
    }

    public List<SearchResult> getSearchResults() {
        return searchResults;
    }

    public StatusModel getStatus() {
        return status;
    }

}
