package net.mahes.chackup;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

public class CmdArguments {

    public CmdArguments(String[] argv) {
        JCommander.newBuilder()
                .addObject(this)
                .build()
                .parse(argv);
    }

    @Parameter(names = {"--webport", "-p"}, description = "Port for the webserver to run on")
    int webPort = 8080;

    @Parameter(names = {"--webhost", "-h"}, description = "Host restriction for the webserver")
    String webHost = "0.0.0.0";

    @Parameter(names = {"--jmxport"}, description = "Port for the JMX server to run on")
    int jmxPort = 9876;

    @Parameter(names = {"--indexdata", "-i"}, description = "Debug mode")
    boolean indexExistingData = false;

    @Parameter(names = {"--jmx", "-j"}, description = "Start JMX Server")
    boolean startJmxServer = false;

    @Parameter(names = {"--web", "-w"}, description = "Start Web Server")
    boolean startWebServer = false;

    @Parameter(names = {"--updater", "-u"}, description = "Start Updater")
    boolean startUpdater = false;

    @Parameter(names = {"--downloader", "-d"}, description = "Start Downloader")
    boolean startDownloader = false;

}
