package net.mahes.chackup.model;

import java.util.List;

public class ThreadsResult {
    private List<Page> result;

    public List<Page> getResult() {
        return result;
    }

    public void setResult(List<Page> result) {
        this.result = result;
    }

}
