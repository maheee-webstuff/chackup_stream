package net.mahes.chackup.model;

public class Post {

    private long no;
    private String now;
    private String name;
    private String com;
    private String filename;
    private String ext;
    private long tim;
    private long time;
    private String md5;
    private String semantic_url;

    private long threadNo;

    public Post() {
    }

    public long getNo() {
        return no;
    }

    public void setNo(long no) {
        this.no = no;
    }

    public String getNow() {
        return now;
    }

    public void setNow(String now) {
        this.now = now;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCom() {
        return com;
    }

    public void setCom(String com) {
        this.com = com;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public long getTim() {
        return tim;
    }

    public void setTim(long tim) {
        this.tim = tim;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public String getSemantic_url() {
        return semantic_url;
    }

    public void setSemantic_url(String semantic_url) {
        this.semantic_url = semantic_url;
    }

    public long getThreadNo() {
        return threadNo;
    }

    public void setThreadNo(long threadNo) {
        this.threadNo = threadNo;
    }

}
