package net.mahes.chackup;

import javax.inject.Singleton;

@Singleton
public class Config {

    public final int BACKLOG_POLL_TIMEOUT = 2 * 1000; // ms
    
    public final int UPDATER_REQUEST_PAUSE = 0; // ms
    public final int UPDATER_RUN_PAUSE = 60 * 1000; // ms
    public final int DOWNLOADER_RUN_PAUSE = 0; // ms
    
    public final int MAX_RESULTS_SEARCH = 100;
    public final int MAX_RESULTS_NEWEST = 100;
    public final int MAX_RESULTS_OVERVIEW = 210;
    
    public final int DOWNLOADER_CONNECT_TIMEOUT = 10 * 1000; //ms
    public final int DOWNLOADER_READ_TIMEOUT = 120 * 1000; //ms

    public final int UPDATER_CONNECT_TIMEOUT = 10 * 1000; //ms
    public final int UPDATER_READ_TIMEOUT = 10 * 1000; //ms

    public final String FOLDER_TEMPLATES = "net/mahes/chackup/resources/templates";
    public final String FOLDER_WEBASSETS = "net/mahes/chackup/resources/webassets";

    public final String FOLDER_DATA = "var/data/";
    public final String FOLDER_THREADS = "var/threads/";
    public final String FOLDER_IMAGES = "var/images/";

    public final String BOARD = "b";

    public final String URL_BASE = "http://a.4cdn.org/";
    public final String URL_THREADS = "/threads.json";
    public final String URL_THREAD_PRE = "/thread/";
    public final String URL_THREAD_SUFF = ".json";
    public final String URL_IMAGE_PRE = "http://i.4cdn.org/";

    public final String CONTROL_PARAMETER_SERVER = "server";
    public final String CONTROL_PARAMETER_UPDATER = "updater";
    public final String CONTROL_PARAMETER_DOWNLOADER = "downloader";
    
    public final String CONTROL_PARAMETER_START = "start";
    public final String CONTROL_PARAMETER_STOP = "stop";
}
