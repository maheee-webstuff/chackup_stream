package net.mahes.chackup.services;

import javax.inject.Inject;
import javax.inject.Singleton;

import net.mahes.chackup.Config;

@Singleton
public class UrlBuilder {

    private Config config;
    
    @Inject
    public UrlBuilder(Config config) {
        this.config = config;
    }
    
    public String getThreadsUrl(String board) {
        return config.URL_BASE + board + config.URL_THREADS;
    }

    public String getThreadUrl(String board, String id) {
        return config.URL_BASE + board + config.URL_THREAD_PRE + id + config.URL_THREAD_SUFF;
    }
    
    public String getImageUrl(String board, long tim, String ext) {
        return config.URL_IMAGE_PRE + board + "/" + tim + ext;
    }

}
