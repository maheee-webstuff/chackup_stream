package net.mahes.chackup.services;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.pmw.tinylog.Logger;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import net.mahes.chackup.model.ThreadResult;
import net.mahes.chackup.model.ThreadsResult;

@Singleton
public class Requester {

    private UrlBuilder urlBuilder;

    @Inject
    public Requester(UrlBuilder urlBuilder) {
        this.urlBuilder = urlBuilder;
    }

    public ThreadsResult getThreads(String board) {
        try {
            return Unirest.get(urlBuilder.getThreadsUrl(board)).asObject(ThreadsResult.class).getBody();
        } catch (UnirestException e) {
            Logger.warn("Failed to download thread list " + board);
            Logger.trace(e);
            throw new RuntimeException(e);
        }
    }

    public ThreadResult getThread(String board, long id) {
        try {
            return Unirest.get(urlBuilder.getThreadUrl(board, "" + id)).asObject(ThreadResult.class).getBody();
        } catch (UnirestException e) {
            Logger.warn("Failed to download thread " + board + "/" + id);
            Logger.trace(e);
            throw new RuntimeException(e);
        }
    }

}
