package net.mahes.chackup.services.worker;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.pmw.tinylog.Logger;

import net.mahes.chackup.model.Post;
import net.mahes.chackup.services.Backlog;
import net.mahes.chackup.services.ImageDownloader;

@Singleton
public class PostWorker extends AbstractWorker {

    private Backlog backlog;
    private ImageDownloader imageDownloader;

    @Inject
    public PostWorker(Backlog backlog, ImageDownloader imageDownloader) {
        this.backlog = backlog;
        this.imageDownloader = imageDownloader;
    }

    public boolean handleNextPost() {
        Logger.info("Downloading next Image ...");
        Post post = backlog.takePost();
        if (post == null) {
            Logger.info("No Image found! Backlog empty!");
            return false;
        }

        Logger.info("Downloading Image {}/{}{} ...", post.getThreadNo(), post.getNo(), post.getExt());

        imageDownloader.downloadImage(post);

        return true;
    }

}
