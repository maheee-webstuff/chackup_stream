package net.mahes.chackup.services.worker;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.pmw.tinylog.Logger;

import net.mahes.chackup.Config;
import net.mahes.chackup.model.Page;
import net.mahes.chackup.model.ThreadModel;
import net.mahes.chackup.model.ThreadsResult;
import net.mahes.chackup.services.Backlog;
import net.mahes.chackup.services.Requester;

@Singleton
public class ThreadlistWorker extends AbstractWorker {

    private Requester requester;
    private Backlog backlog;

    private String board;
    private long lastRun = 0; // timestamp of last run in seconds

    @Inject
    public ThreadlistWorker(Config config, Requester requester, Backlog backlog) {
        this.requester = requester;
        this.backlog = backlog;

        board = config.BOARD;
    }

    public void refreshThreadList() {
        Logger.info("Refreshing Thread List ...");

        ThreadsResult threadsResult = requester.getThreads(board);

        long maxTimestamp = lastRun;

        for (Page page : threadsResult.getResult()) {
            for (ThreadModel thread : page.getThreads()) {
                if (thread.getLast_modified() > lastRun) {
                    backlog.put(thread);
                }
                maxTimestamp = Math.max(maxTimestamp, thread.getLast_modified());
            }
        }
    }

    public long getLastRun() {
        return lastRun;
    }

    public void setLastRun(long lastRun) {
        this.lastRun = lastRun;
    }

}
