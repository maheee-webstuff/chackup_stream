package net.mahes.chackup.services;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.j256.simplejmx.common.JmxAttributeMethod;
import com.j256.simplejmx.common.JmxResource;

import net.mahes.chackup.Config;
import net.mahes.chackup.model.Post;
import net.mahes.chackup.model.ThreadModel;

@Singleton
@JmxResource(domainName = "chackup")
public class Backlog {

    private Config config;

    private LinkedBlockingQueue<ThreadModel> threadBacklog;
    private LinkedBlockingQueue<Long> threadImageDownloadIdBacklog;
    private LinkedBlockingQueue<Post> postBacklog;
    private LinkedBlockingQueue<Post> priorityPostBacklog;

    @Inject
    public Backlog(Config config) {
        this.config = config;

        threadBacklog = new LinkedBlockingQueue<>();
        threadImageDownloadIdBacklog = new LinkedBlockingQueue<>();
        postBacklog = new LinkedBlockingQueue<>();
        priorityPostBacklog = new LinkedBlockingQueue<>();
    }

    public void put(ThreadModel thread) {
        threadBacklog.add(thread);
    }

    public void put(Long threadId) {
        threadImageDownloadIdBacklog.add(threadId);
    }
    
    public void put(ThreadModel thread, Post post) {
        put(thread, post, false);
    }

    public void put(ThreadModel thread, Post post, boolean priority) {
        post.setThreadNo(thread.getNo());
        if (priority) {
            priorityPostBacklog.add(post);
        } else {
            postBacklog.add(post);
        }
    }

    public ThreadModel takeThread() {
        try {
            return threadBacklog.poll(config.BACKLOG_POLL_TIMEOUT, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            return null;
        }
    }
    
    public Long takeThreadImageDownloadId() {
        try {
            return threadImageDownloadIdBacklog.poll(config.BACKLOG_POLL_TIMEOUT, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            return null;
        }
    }

    public Post takePost() {
        try {
            Post prioPost = priorityPostBacklog.poll();
            if (prioPost != null) {
                return prioPost;
            } else {
                return postBacklog.poll(config.BACKLOG_POLL_TIMEOUT, TimeUnit.MILLISECONDS);
            }
        } catch (InterruptedException e) {
            return null;
        }
    }

    @JmxAttributeMethod()
    public int getThreadBacklogSize() {
        return threadBacklog.size();
    }

    @JmxAttributeMethod()
    public int getPostBacklogSize() {
        return priorityPostBacklog.size() + postBacklog.size();
    }

}
