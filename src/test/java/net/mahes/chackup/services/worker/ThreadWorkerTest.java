package net.mahes.chackup.services.worker;

import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import net.mahes.chackup.Config;
import net.mahes.chackup.model.Post;
import net.mahes.chackup.model.ThreadModel;
import net.mahes.chackup.server.search.Search;
import net.mahes.chackup.services.Backlog;
import net.mahes.chackup.services.Requester;
import net.mahes.chackup.services.Storage;

@RunWith(value = MockitoJUnitRunner.class)
public class ThreadWorkerTest {

    private Config config = new Config();

    @Mock
    private Requester requester;

    @Mock
    private Backlog backlog;

    @Mock
    private Storage storage;

    @Mock
    private Search search;

    private ThreadWorker threadWorker;

    @Before
    public void setUp() {
        threadWorker = new ThreadWorker(config, requester, backlog, storage, search);
    }

    @Test
    public void mergePostsTest() {
        // given
        ThreadModel thread = new ThreadModel();
        List<Post> oldPosts = new LinkedList<Post>();
        List<Post> newPosts = new LinkedList<Post>();

        thread.setImagesLoaded(true);

        oldPosts.add(createPost(7L));
        oldPosts.add(createPost(1L));
        oldPosts.add(createPost(3L));
        oldPosts.add(createPost(5L));

        newPosts.add(createPost(5L));
        newPosts.add(createPost(4L));
        newPosts.add(createPost(6L));
        newPosts.add(createPost(8L));

        // when
        List<Post> result = threadWorker.mergePosts(thread, oldPosts, newPosts);

        // then
        Assert.assertEquals(7, result.size());

        Assert.assertEquals(1L, result.get(0).getNo());
        Assert.assertEquals(3L, result.get(1).getNo());
        Assert.assertEquals(4L, result.get(2).getNo());
        Assert.assertEquals(5L, result.get(3).getNo());
        Assert.assertEquals(6L, result.get(4).getNo());
        Assert.assertEquals(7L, result.get(5).getNo());
        Assert.assertEquals(8L, result.get(6).getNo());

        Mockito.verify(backlog).put(thread, result.get(2), false);
        Mockito.verify(backlog).put(thread, result.get(4), false);
        Mockito.verify(backlog).put(thread, result.get(6), false);

        Mockito.verifyNoMoreInteractions(backlog);
    }

    private Post createPost(long no) {
        Post post = new Post();
        post.setNo(no);
        post.setExt(".png");
        return post;
    }

}
